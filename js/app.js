$(document).ready(function() {

  var owl = $("#owl-demo");
  owl.owlCarousel({
    rtl:true,
    loop:true,
      items : 10, //10 items above 1000px browser width
      itemsDesktop : [1000,5], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,5], // betweem 900px and 601px
      itemsTablet: [600,3], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
      autoPlay: true
    });

  // Custom Navigation Events
  $(".next").click(function(){
    owl.trigger('owl.next');
  })
  $(".prev").click(function(){
    owl.trigger('owl.prev');
  })

  $(".demo1").bootstrapNews({
    newsPerPage: 3,
    autoplay: true,
    pauseOnHover:true,
    direction: 'up',
    newsTickerInterval: 4000,
    onToDo: function () {
      //console.log(this);
    }
  });
  function blinker() {
    $('.blink_me').fadeOut(500);
    $('.blink_me').fadeIn(500);
}

setInterval(blinker, 1000);
});